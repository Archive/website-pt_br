<? include ('topo.inc') ?>

<div align="justify">
<p>
Esse � o projeto de tradu��o do GNOME (GNU Network Object
Model Environment). Nossos coordenadores atuais s�o Gustavo
Noronha Silva &lt;<a href="mailto:kov@debian.org">kov@debian.org</a>&gt;,
Evandro Fernandes Giovanini &lt;<a href="mailto:evandrofg@ig.com.br">evandrofg@ig.com.br</a>&gt; e
Gustavo Maciel Dias Vieira 
&lt;<a href="mailto:gdvieira@zaz.com.br">gdvieira@zaz.com.br</a>&gt;.
<p>
Se voc� quer ajudar, inscreva-se na <a
href="http://listas.cipsga.org.br/cgi-bin/mailman/listinfo/gnome-l10n-br">lista 
de discuss�es do GNOME-l10n-BR</a>
e envie um email de apresenta��o, dizendo em que pretende ajudar.
Leia tamb�m o nosso <a href="ajudar.php">pequeno manual sobre como
ajudar</a>.
<p>
Procure sempre manter o coordenador informado das suas atividades.
Isso garante o bom andamento do processo de tradu��o e evita esfor�o
duplicado. Informe a lista tamb�m, quando necess�rio.
<p>
Para informa��es sobre como ajudar com tradu��es de outros programas
e outras informa��es �teis que s�o necess�rias para os tradutores visite
<a href="http://www.iro.umontreal.ca/contrib/po/HTML/index.html">o
site do Translation Project</a>. Veja informa��es sobre o <a 
href="http://www2.iro.umontreal.ca/~pinard/po/registry.cgi?team=pt_BR">time 
brasileiro</a>.

<p>Alguns links interessantes: 
<ul>
 <li><a href="http://www.gnome.org/">Site oficial do GNOME</a>.</li> 
 <li><a href="http://developer.gnome.org/projects/gtp/">Projeto de 
     Tradu��o do GNOME</a> (do qual fazemos parte).</li> 
 <li><a href="http://developer.gnome.org/projects/gdp/">Projeto de 
     Documenta��o do GNOME</a>.</li>
 <li><a href="http://debian-br.cipsga.org.br/resumo-gnome/">Resumos 
     GNOME: os GNOME-Summaries traduzidos.</a> </li>
</ul>

<p>Situa��o Atual do Nosso Trabalho no <b>GNOME 1.4</b>:
<ul>
 <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-1.4/pt_BR/core/index.html">Situa��o das aplica��es principais</a></li>
 <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-1.4/pt_BR/fifth-toe/index.html">Situa��o das aplica��es adicionais</a></li>
 <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-1.4/pt_BR/extras/index.html">Situa��o das aplica��es extras</a></li>
</ul>

<p>Situa��o Atual do Nosso Trabalho no <b>GNOME 2.2</b>:
<ul>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.2/pt_BR/desktop/index.html">Situa��o das aplica��es do desktop</a></li>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.2/pt_BR/developer-libs/index.html">Situa��o das bibliotecas de desenvolvimento</a></li>
</ul>

<p>Situa��o Atual do Nosso Trabalho no <b>GNOME 2.4</b>:
<ul>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.4/pt_BR/desktop/index.html">Situa��o das aplica��es do desktop</a></li>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.4/pt_BR/fifth-toe/index.html">Situa��o das aplica��es adicionais</a></li>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.4/pt_BR/extras/index.html">Situa��o das aplica��es extras</a></li>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.4/pt_BR/office/index.html">Situa��o das aplica��es do office</a></li>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.4/pt_BR/developer-app/index.html">Situa��o das aplica��es de desenvolvimento</a></li>
  <li><a href="http://developer.gnome.org/projects/gtp/status/gnome-2.4/pt_BR/developer-libs/index.html">Situa��o das bibliotecas de desenvolvimento</a></li>
</ul>

<? include ('fim.inc') ?>
